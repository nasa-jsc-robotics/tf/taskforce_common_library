Change Log
==========

2.1.1
-----

* Added SetParameter task

2.1.0
-----

* Removed old .block and .block.scene files
* moved the "blocks" folder to "groups" folder
* Added LICENSE.md

2.0.0
-----

* Taskforce 2 changes
* Added Empty Task
* Removed trusty CI


1.1.1
-----

* Added xenial CI
