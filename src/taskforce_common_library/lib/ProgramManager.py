from threading import Thread
import Queue
import subprocess
import time
import shlex

import logging
logger = logging.getLogger(__name__)

DEFAULT_END_PROCESS_TIMEOUT = 3

def enqueue_output(out, queue):
    """ Helper method to handle another thread's stdout/stderr

    Args:
        out: output of the subprocess (i.e. stdout, stderr)
        queue: Queue object to pass the output of the process
    """
    for line in iter(out.readline, b''):
        queue.put(line)
    out.close()


class ProgramManager(object):

    def __init__(self, terminalString='terminator -e'):
        self.programs = dict()
        self.terminalString = terminalString

    def startProgram(self, callstring, programKey, endProcessTimeout=DEFAULT_END_PROCESS_TIMEOUT, log=False):
        """ Start a new process

        Args:
            callstring: string that will be sent to the commandline
            programKey: key used to label the process as a string
            endProcessTimeout: Max wait-time before the process will be sent a kill (equivalent to a kill -9 in Linux)
                                  If less than zero, then the process will wait forever for an exit.
            log: Capture the stdout of the subprocess and log output at loglevel logging.INFO
        """
        if programKey in self.programs.keys():
            if self.getProgramStatus(programKey) == 'alive':
                return
        prog = Program(shlex.split(callstring), programKey, endProcessTimeout, log)
        self.programs[programKey] = prog
        try:
            prog.start()
        except Exception as e:
            logger.error(e, exc_info=True)

    def stopProgram(self, programKey):
        """ Stop a process that is already running and managed

        Args:
            programKey: Label used for the process
        """
        try:
            self.programs[programKey].stop()
        except KeyError:
            msg = 'Unable to stop program ' + programKey + ' because it does not exist.'
            logger.error(msg)

    def getProgramStatus(self, programKey):
        """ Get the status of a process

        Args:
            programKey: Label used for the process

        Returns:
            status as a string: 'not_found', 'dead', or 'alive'
        """
        if programKey not in self.programs.keys():
            return 'not_found'
        status = self.programs[programKey].is_alive()
        if status:
            return 'alive'
        else:
            return 'dead'

    def getPid(self, programKey):
        """ Return the PID of a process

        Args:
            programKey: label used for the process

        Returns:
            Process ID as an integer
        """
        return self.programs[programKey].pid

    def __del__(self):
        for program in self.programs.values():
            program.stop()


class Program(Thread):

    """
    This class encapsulates the program in a thread.  This class sets up passing stdout/stderr out of the program
    to a logger.
    """

    def __init__(self, cmd, programName='', endProcessTimeout=DEFAULT_END_PROCESS_TIMEOUT, log=False):
        """Constructor

        Args:
            cmd: command line string as a string
            programName: name of the program as a string.  Used to namespace the logger.
            endProcessTimeout: maximum wait time to allow the process to stop after the "stop" method is called.
                               If this timeout value is exceeded, a SIGINT is sent to the process.  Setting to a
                               negative value for no timeout (wait forever)
            log: Flag of whether or not to log as a boolean
        """
        Thread.__init__(self)
        self.daemon = True
        self.exit = False
        self.cmd = cmd
        self.programName = programName
        self.log = log
        self.logger = logger.getChild(programName)
        self.endProcessTimeout = endProcessTimeout
        self._pid = None

    @property
    def pid(self):
        return self._pid

    def run(self):
        """ Overload for the Thread's 'run' method.

        This method get's called when the Thread's "start" method is called.  This method will execute the command
        passed into the constructor in a new subprocess.  While running, this method will pass stdout and stderr
        to the logger.
        """

        if self.log:
            p = subprocess.Popen(self.cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True)

            # Create a Queue for the processes stdout and stderr
            q = Queue.Queue()
            output_thread = Thread(target=enqueue_output, args=(p.stdout, q))
            output_thread.daemon = True  # thread dies with the program
            output_thread.start()
        else:
            p = subprocess.Popen(self.cmd, stderr=subprocess.STDOUT)

        self._pid = p.pid

        while not self.exit and p.poll() is None:
            time.sleep(0.1)
            if self.log:
                try:
                    line = q.get_nowait()  # or q.get(timeout=.1)
                    if line != '':
                        self.logger.info(line)
                except Queue.Empty:
                    pass

        if self.log:
            # once the process is complete, or asked to exit, empty out the log queue
            while not q.empty():
                try:
                    line = q.get_nowait()  # or q.get(timeout=.1)
                    if line != '':
                        self.logger.info(line)
                except Queue.Empty:
                    pass

        try:
            self.logger.info('terminating...')
            # ask process to terminate (sends a signal.SIGTERM)
            p.terminate()
        except OSError:
            pass

        # Timeout logic used to force an exist
        if self.endProcessTimeout >= 0:

            start = time.time()
            while (time.time() - start) < self.endProcessTimeout and p.poll() == None:
                time.sleep(0.1)

            # process is still running after timeout, send kill
            if p.poll() == None:
                self.logger.warn('Process not responding to SIGTERM, raising to SIGKILL')
                p.kill()

        else:
            while p.poll() is None:
                time.sleep(0.1)

        # wait for the process to complete
        p.wait()

    def stop(self):
        """ Stop the program."""
        self.exit = True
