from taskforce_common import Task, TaskState, Response, ErrorResponse

from taskforce_common_library.lib.ProgramManager import ProgramManager

class ProgramManagerTask(Task):
    """
    The ProgramManager Task is a wrapper for the ProgramManager class in taskforce_common_library.lib.ProgramManager
    
    The purpose of the ProgramManager is to start and stop long-running processes.  The only purpose of this task
    is to provide commands to the underlying program manager.  The "standard" TaskForce commands (init, start, stop, etc),
    have no effect.
    """
    
    events = []
    commands = ['startProgram', 'stopProgram']
    parameters = {}

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)
        self.programManager = ProgramManager()
        
    def startProgram(self, callstring, programName):
        return self.programManager.startProgram(callstring, programName)
        
    def stopProgram(self, programName):
        return self.programManager.stopProgram(programName)
