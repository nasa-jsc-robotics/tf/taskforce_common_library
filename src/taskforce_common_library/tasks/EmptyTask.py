from taskforce_common import Task, TaskState, Response, ErrorResponse


class EmptyTask(Task):
    """
    The EmptyTask does not really do anything.  It is useful in places where a callable 
    is needed to trigger other Tasks, or for testing.  It could be used as a place holder
    as well.
    """
    events = []
    commands = []
    parameters = {}

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self, name, commandPorts)

        ########################
        #  INSERT USER CODE HERE
        ########################

    def onInit(self):
        """
        The onInit method will get called when the Task is initialized.
        Typically, this only gets called once per deployment.  This is where
        you would register publishers / subscribers, or do one-time setup
        functions. If init is successful, it should return True.
        On error, it should return False.  onInit is also invokable at
        run-time, so place code here that needs to be re-inited, since you
        can't re-run the constructor.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onStartup(self):
        """
        The onStartup method will get called when a Task is started.  This is
        for functions that need to be run every time the Task is run, but
        maybe not in a continuous loop. If onStartup is successful, it should
        return True.  On error, it should return False.
        """
        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onExecute(self):
        """
        The onExecute method is the "main" loop of the Task.  When a task is
        started, if will first call onStartup.  If onStartup is successful,
        the onExecute method will be called in it's own thread.  If a task
        is currently running, more onExecute threads will NOT be created.

        if onExecute needs to run continuously, put the code in following loop:
            while (self.state == TaskState.RUNNING):
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onStop(self):
        """
        The onStop method is called when a task is manually told to stop.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True

    def onShutdown(self):
        """
        The onShutdown method is called when a task is being removed completely.
        Code needed to clean up or special destructors should be placed here.
        """

        ########################
        #  INSERT USER CODE HERE
        ########################

        return True
