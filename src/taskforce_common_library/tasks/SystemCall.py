from taskforce_common import Task
import subprocess
import time

class SystemCall(Task):
    """
    Make a system call.  The 'command' will get called as a python subprocess
    
    Parameters:
        command - the execution string.  This would be what you would enter in a terminal command line.
        startupDelay - Number of seconds to wait between calling "startup" and actually starting the subprocess.
    """
    parameters = {'command':'','startupDelay':0.00}

    def onExecute(self):
        command = self.parameters['command']
        startupDelay = self.parameters['startupDelay']
    
        start_time = time.time()
        while(time.time() - start_time < startupDelay):
            time.sleep(0.1)

        self.logger.info('[{}] syscall:"{}"'.format(self.name,command))
        subprocess.call(self.parameters['command'], shell=True)
        return True