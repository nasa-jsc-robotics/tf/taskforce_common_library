"""
This Task is meant to set a single parameter of another Task.

Rather than using this Task directly, it may be more useful to 
copy this pattern (or variants to your use-case).
"""
from taskforce_common import Task, TaskState, Response, ErrorResponse

class SetParameter(Task):

    events = ['setParameter']
    commands = []
    parameters = {
        'parameter': '<param name>',
        'value' : '<value>'
    }

    def onExecute(self):
        params = {self.parameters['parameter']: self.parameters['value']}
        self.logger.debug('parameters: {}'.format(params))
        
        # By adding a dictionary as the optional 'data' field of the emit method, a dictionary
        # will be sent with the event.  If another Task subscribes to this Task's "setParameter"
        # event, and sets "setParameter" as the callback, the other Task will have it's parameters
        # set.
        self.emit('setParameter', params)
        return True
        