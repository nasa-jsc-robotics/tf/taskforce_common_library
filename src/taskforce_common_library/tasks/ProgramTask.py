from taskforce_common import Task, TaskState, Response, ErrorResponse

import subprocess
import time
import shlex

class ProgramTask(Task):
    """
    The purpose of the ProgramTask is to encapsulate the execution of some other program in a subprocess.

    Events:
        programStarted - emitted after the process has started
        programStopped - emitted when the program has either completed, or stopped.
        
    Parameters:
        command - the execution string.  This would be what you would enter in a terminal command line.
        endProcessTimeout - After this number of seconds, this task will forceably kill the subprocess.
        startupDelay - Number of seconds to wait between calling "startup" and actually starting the subprocess.
        startedDelay - Number of seconds to wait after starting the process before emitting the "programStarted" event.
    """
    events = ['programStarted','programStopped']
    commands = []
    parameters = {'command':'','endProcessTimeout': 3, 'startupDelay': 0.00, 'startedDelay': 0.00}

    def onExecute(self):
        command = shlex.split(self.parameters['command'])
        timeout = self.parameters['endProcessTimeout']
        startupDelay = self.parameters['startupDelay']
        startedDelay = self.parameters['startedDelay']
        
        self.logger.info('({}) - executing:{}'.format(self.name,command))
        
        # Wait startupDelay before actually starting the process
        start_time = time.time()
        while(time.time() - start_time < startupDelay):
            time.sleep(0.1)
        
        # Open a subprocess and start it
        try:
            p = subprocess.Popen(command, stderr=subprocess.STDOUT)
            pid = p.pid
        except Exception as e:
            self.logger.error("Exception when executing code: '{}'".format(e.message))
            return False
        
        # Wait startedDelay before emitting the programStarted event
        start_time = time.time()
        while(time.time() - start_time < startedDelay):
            time.sleep(0.1)

        # Emit that the program has started
        self.emit('programStarted')
        
        while (self.state == TaskState.RUNNING and p.poll() is None):
            time.sleep(0.1)

        try:
            self.logger.info('({}) terminating program...'.format(self.name))
            p.terminate()
        except OSError:
            pass
            
        if timeout >= 0:
            start = time.time()
            while (time.time() - start) < timeout and p.poll() == None:
                time.sleep(0.1)

            # process is still running after timeout, send kill
            if p.poll() == None:
                self.logger.warn('Process not responding to SIGTERM, raising to SIGKILL')
                p.kill()
                
        else:
            while p.poll() is None:
                time.sleep(0.1)

        p.wait()        
        self.emit('programStopped')
        return True
        
    def onShutdown(self):
        self.stop()
        
