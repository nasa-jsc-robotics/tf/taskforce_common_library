# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse

class EventCounter(Task):
    """
    The EventCounter acts as a way of counting a certain number of events, and then emitting a signal.
    
    This Task does not do anything when "started".  Instead, it subscribes to events that are to be counted.
    Use the "increment" method the callback.  Once the "goalCount" has been reached, this task will emit the
    "goalReached" event.  When EventCounter is incremented, if the goal has NOT been reached, the 
    "goalNotReached" event will be emitted.
    
    Events:
        goalReached - emitted when the count is checked and has reached "goalCount".
        goalNotReached - emitted when the count is checked and the counter has NOT reached "goalCount"

    Commands:
        increment - Increment the counter.  This triggers checkForGoal.
        reset - reset the counter
        checkForGoal - check the increment count.  If the goal has been reached, reset the count.
    """
    
    events = ['goalReached', 'goalNotReached']
    commands = ['increment','reset','checkForGoal']
    parameters = {'goalCount' : 1}

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)
        self.count = 0
        self.customStatus = '{}:{}'.format(self.count,self.parameters['goalCount'])
        
    def increment(self):
        self.logger.debug('[{}] increment')
        self.count += 1
        self.checkForGoal()
    
    def reset(self):
        self.logger.debug('[{}] reset')
        self.count = 0
        self.customStatus = '{}:{}'.format(self.count,self.parameters['goalCount'])
        
    def checkForGoal(self):
        self.customStatus = '{}:{}'.format(self.count,self.parameters['goalCount'])
        if self.count >= self.parameters['goalCount']:
            self.logger.debug('[{}] goalReached!')
            self.emit('goalReached')
            self.reset()
        else:
            self.logger.debug('[{}] goalNotReached!')
            self.emit('goalNotReached')
            
    def setParameter(self, keyValues={}):
        Task.setParameter(self, keyValues)
        self.checkForGoal()
