from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['taskforce_common_library',
              'taskforce_common_library.lib'],
    package_dir={'': 'src'}
)

setup(**d)
